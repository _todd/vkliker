# coding: utf-8 -*-
# python 3.3.0

from urllib.request import urlopen, build_opener, \
                           HTTPHandler, HTTPRedirectHandler, \
                           HTTPCookieProcessor
from http.cookiejar import CookieJar


class HTTPClient(object):
    USER_AGENT = "Mozilla/5.0 (X11; U; Linux i686) Gecko/20071127 Firefox/2.0.0.11"
    
    def __init__(self):
        self.http_handler = HTTPHandler()
        self.redirect_handler = HTTPRedirectHandler()
        self.cookiejar = CookieJar()
        self.cookie_handler = HTTPCookieProcessor(self.cookiejar)
        
        self.opener = build_opener(
            self.http_handler,
            self.redirect_handler,
            self.cookie_handler,
        )
            
        self.opener.addheaders = [
            ("User-agent", HTTPClient.USER_AGENT),
        ]
        
    def request(self, url, params=b""):
        if params:
            return self.opener.open(url, params)
        return self.opener.open(url)
        
    #def get_cookies(self):
    #    return self.cookiejar._cookies