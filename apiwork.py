# -*- coding: utf-8 -*-
# python 3.3.0

import json
import time
import getpass
from urllib.parse import urlencode

import vk_auth
from http_client import HTTPClient
client = HTTPClient()


class VKApiWork(object):
    def __init__(self, config, email=None, password=None):
        self.API_URL   = config.get("GlobalSettings", "API_URL")
        self.CLIENT_ID = config.get("GlobalSettings", "CLIENT_ID")
        self.DELAY  = int(config.get("GlobalSettings", "DELAY").split(";")[0])
        self.AMOUNT = int(config.get("GlobalSettings", "AMOUNT").split(";")[0])
        self.get_auth(email, password)
        
    
    def get_auth(self, email=None, password=None):
        if not (email and password):
            email = input("Your email or phone: ")
            password = getpass.getpass()
            
        self.scope = ["photos", "wall", ]
        
        try:
            self.token, self.user_id = \
                vk_auth.auth(email, password, self.CLIENT_ID, self.scope)
        except Exception as ex:
            raise RuntimeError("Ошибка авторизации")
            
        user_info = self.get_userInfo()
        print("Здравствуйте, {0} {1}.".format(
            user_info.get("last_name", ""), user_info.get("first_name", "")))
    
    def call_api(self, method, params):
        params["access_token"] = self.token
        url = self.API_URL.format(method, urlencode(params))
        response = client.request(url)
        answer = json.loads(response.read().decode("utf-8"))
        return answer.get("response", answer.get("error", None))        
    
    def get_userInfo(self, user_id=None):
        user_id = user_id if user_id else self.user_id
        
        method = "users.get"
        params = {
            "user_ids": user_id,
            "fields": "first_name, last_name",
            "name_case": "nom",
        }
        response = self.call_api(method, params)
        
        if (isinstance(response, dict)):
            raise RuntimeError("Произошла ошибка: ", response["error_msg"])
        return response[0]
        
    def get_groupInfo(self, user_id=None):
        user_id = user_id if user_id else self.user_id
        
        method = "groups.getById"
        params = {
            "group_id": user_id,
        }
        response = self.call_api(method, params)
        if (isinstance(response, dict)):
            raise RuntimeError("Произошла ошибка: ", response["error_msg"])
        return response[0]
        
    def get_validId(self, user_id):
        try:
            return self.get_userInfo(user_id)["uid"]
        except RuntimeError:
            if (user_id.startswith("public")):
                user_id = user_id[6:]
            return -self.get_groupInfo(user_id)["gid"]
        except:
            raise RuntimeError("Invalid user id")
        
        
    def get_(self, method, params, user_id):
        max_count = params.get("count", 100)
        
        response = self.call_api(method, params)
        result = []
        result.extend(response[1:])
    
        count = 0
        total_count = response[0]
        current_count = len(result)
        while (total_count - current_count > 0):
            params["offset"] += max_count
            response = self.call_api(method, params)
            result.extend(response[1:])
            current_count = len(result)
            count += 1
            if (count % self.AMOUNT == 0):
                time.sleep(self.DELAY)
                
        return (total_count, result)
    
    
class ApiLiker(VKApiWork):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
    # фотки
    def get_photos(self, user_id):
        method = "photos.getAll"
        params = {
            "owner_id": user_id,
            "count": 200,
            "offset": 0,
            "extended": 1,
        }
        return self.get_(method, params, params["owner_id"])
    
    # нелайкнутые фотки
    def get_unlikedPhotos(self, photos):
        return (photo["pid"] for photo in photos if not photo["likes"]["user_likes"])
    # ----
    
    # посты
    def get_posts(self, user_id):
        method = "wall.get"
        params = {
            "owner_id": user_id,
            "count": 100,
            "offset": 0,
            "filter": "all",
        }
        return self.get_(method, params, user_id)
                
    # нелайкнутые посты    
    def get_unlikedPosts(self, posts):
        return (post["id"] for post in posts if not post["likes"]["user_likes"])
    # ----
    
    def add_likes(self, ids, user_id, type="photo"):
  
        hwidth = 39
        header = "|   № |            ID |         LIKES |"
        print("+{0}+\n{1}\n+{0}+".format("-"*(hwidth-2), header))
      
        method = "likes.add"
        params = {
            "owner_id": user_id,
            "type": type
        }
        
        count = 0
        for lid in ids:
            params["item_id"] = lid
            res = self.call_api(method, params)
            print("|{0:>4} |{1:>14} |{2:>14} |".format(
                    count, lid, res.get("likes", None)))
            count += 1
            if (count % self.AMOUNT == 0):
                time.sleep(self.DELAY)
                
        print("+{}+".format("-"*(hwidth-2)))
                
                
    # Добавление лайков к фотографиям
    def add_likesToPhotos(self, user_id=-1):
        user_id = self.get_validId(user_id)
        pcount, photos = self.get_photos(user_id)
        unliked = self.get_unlikedPhotos(photos)
        self.add_likes(unliked, user_id, "photo")
        
    # Добавление лайков к постам
    def add_likesToPosts(self, user_id=-1):
        user_id = self.get_validId(user_id)
        pcount, posts = self.get_posts(user_id)
        unliked = self.get_unlikedPosts(posts)
        self.add_likes(unliked, user_id, "post")