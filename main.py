# -*- coding: utf-8 -*-
# python 3.3.0

from configparser import ConfigParser
from apiwork import ApiLiker


def main():
    config = ConfigParser()
    config.read("settings.cfg")
    liker = ApiLiker(config)
    
    acts = {
        "0": liker.add_likesToPhotos,
        "1": liker.add_likesToPosts,
    }
    
    print()
    print("stop для выхода")
    while True:
        ltype = ""
        while (not ltype in ["stop", "0", "1"]):
            ltype = input("Фотки/Посты (0/1): ")
        
        user_id = input("Имя пользователя/группы: ")
        try:
            if (ltype == "stop"):
                break
                
            act = acts.get(ltype, None)
            if act:
                act(user_id)
            else:
                print("Ошибка: пункт не найден")
                
        except:
            print("Произошла ошибка")
            continue
            
if __name__ == "__main__":
    main()
    input()